import { Bot, InputFile} from "grammy";
import { Message } from '@grammyjs/types';
import env from './env';
import { hydrateReply } from "@grammyjs/parse-mode";
import type { ParseModeContext } from "@grammyjs/parse-mode";
import fs from 'fs'

var map: {[key:string]:any} = {};

map['DomandeLaide'] = JSON.parse('' + fs.readFileSync('./DomandeLaide.json'))
map['PreferisciLaido'] = JSON.parse('' + fs.readFileSync('./PreferisciLaido.json'))
map['Warns'] = JSON.parse('' + fs.readFileSync('./Warns.json'))
map['Sputtana'] = JSON.parse('' + fs.readFileSync('./Sputtana.json'))
// Create an instance of the `Bot` class and pass your bot token to it.
const bot = new Bot<ParseModeContext>(env.BOT_TOKEN); // <-- put your bot token between the ""
bot.use(hydrateReply);
//bot.api.config.use(parseMode("MarkdownV2"));
function GetRandom(max: number): number{
    return Math.floor(Math.random() * Math.floor(max))
}

function GetFrase(array: any, filename: string): any{
    var length = Object.keys(array).length -1
    var value = GetRandom(length)
    if (array[value].utilizzi && array[value].utilizzi > array[length].utilizzi){
        var minVal = array[length].utilizzi
        var i = 1
        while (array[i].utilizzi > minVal){
            i += 1
        }
        if (i != length){
            value = i
        }
        else{
            array[length].utilizzi += 1
        }
    }
    var text = array[value].frase
    array[value].utilizzi = array[value].utilizzi == undefined ? 1 : array[value].utilizzi+1
    fs.writeFileSync(filename + ".json", JSON.stringify(array, null, 4))
    map['' + filename]= JSON.parse('' + fs.readFileSync(filename + '.json'))
    return text
}

function RingraziaRandom(text: string): string{
    if (GetRandom(100) <= 7) {
        text = text + "\n\nMolte delle domande di questo bot sono fornite da @BonciBo, ringrazialo!"
    }
    if (GetRandom(100) <= 7) {
        text = text + "\n\nQuesto bot è sviluppato da @Anulo, ringrazialo!"
    }
    return text
}
function ChatIsAllowed(id: number): Boolean{
    if (id==-1001060910179 || id==452798212){
        return true
    }
    return false
}
bot.command("help", async (ctx) => {
    await ctx.reply("Questo bot ha varie funzionalità, per ora quelle disponibili sono:\n /domandalaida - alla scoperta del tuo lato hot 😏", {
        reply_to_message_id: ctx.msg.message_id,
    })
})
bot.command("start", async (ctx) => {
    await ctx.reply("Benvenuto in questo bot dalle disparate funzionalità.\n Scrivi /help per saperne di più", {
        reply_to_message_id: ctx.msg.message_id,
    })
})
bot.command("domandalaida", async (ctx) =>{
    var text = GetFrase(map["DomandeLaide"], "DomandeLaide")
    text = RingraziaRandom(text)
    await ctx.reply(text, {
        reply_to_message_id: ctx.msg.message_id,
    })
})
function PickImg() : any {
    var files_names:string[]  = []
    var files_types:number[]  = []
    var images = fs.readdirSync('./images')
    images.forEach(element => {
      files_names.push('images/'+element)
      files_types.push(1)
    });
    var gifs = fs.readdirSync('./gifs')
    gifs.forEach(element => {
      files_names.push('gifs/'+element)
      files_types.push(2)
    });
    var videos = fs.readdirSync('./videos')
    videos.forEach(element => {
      files_names.push('videos/'+element)
      files_types.push(3)
    });
    let chosenIndex = Math.floor(Math.random() * files_names.length)
    let chosenFile = {
      name: files_names[chosenIndex],
      type: files_types[chosenIndex]
    }

    return chosenFile
}
function Warn(msg: Message): string{
    var text = GetFrase(map["Warns"], "Warns")
    text = " " + text
    if (msg.reply_to_message){
        if (msg.reply_to_message.from){
            var id = String(msg.reply_to_message.from.id)
            if (msg.reply_to_message.from.first_name){
                var buffer = msg.reply_to_message.from.first_name
            }else if (msg.reply_to_message.from.username) {
                var buffer = msg.reply_to_message.from.username
            }else {
                var buffer = id
            }
            text = "<a href=\"tg://user?id=" + id + "\">" + buffer + "</a>" + text
        }
    } else {
        text = "Utente non trovato"
    }
    text = RingraziaRandom(text)
    return text
}
function Cinghia(msg: Message): string{
    var text = " è stato preso a cinghiate!"
    if (msg.reply_to_message){
        if (msg.reply_to_message.from){
            var id = String(msg.reply_to_message.from.id)
            if (msg.reply_to_message.from.first_name){
                var buffer = msg.reply_to_message.from.first_name
            }else if (msg.reply_to_message.from.username) {
                var buffer = msg.reply_to_message.from.username
            }else {
                var buffer = id
            }
            text = "<a href=\"tg://user?id=" + id + "\">" + buffer + "</a>" + text
        }
    } else {
        text = "Utente non trovato"
    }
    text = RingraziaRandom(text)
    return text
}
bot.command("cinghia", async (ctx) =>{
    if (ChatIsAllowed(ctx.msg.chat.id)){
        var text = Cinghia(ctx.msg)
        await ctx.replyWithHTML(text)
    }
})
bot.command("premia", async (ctx) =>{
    if (ChatIsAllowed(ctx.msg.chat.id)){
        var text = Warn(ctx.msg)
        await ctx.replyWithHTML(text)
    }
})
bot.command("punizione", async (ctx) =>{
    if (ChatIsAllowed(ctx.msg.chat.id)){
        var text = Warn(ctx.msg)
        await ctx.replyWithHTML(text)
    }
})
async function sputtana(ctx:any){
  if (ChatIsAllowed(ctx.msg.chat.id)){
      var text:string = ""
      if (!ctx.msg.reply_to_message){
          text = "Utente non trovato"
      }
      text = RingraziaRandom(text)
      if (!ctx.msg.reply_to_message){
          await ctx.replyWithHTML(text)
          return
      }
      var chosenFile = GetFrase(map["Sputtana"], "Sputtana")
      //var chosenFile:any = PickImg()
      if (chosenFile.type == 1) {
        await ctx.replyWithPhoto("https://cagnocammello.com/TelegramBot/"+chosenFile.path, {caption:text})
      }
      else if (chosenFile.type == 2){
        await ctx.replyWithAnimation("https://cagnocammello.com/TelegramBot/"+chosenFile.path, {caption:text})
      }
      else if (chosenFile.type == 3){
        await ctx.replyWithVideo("https://cagnocammello.com/TelegramBot/"+chosenFile.path, {caption:text})
      }

  }
}
async function sputtanaBot(ctx:any){
  if (ChatIsAllowed(ctx.msg.chat.id)){
      var text:string = ""
      text = RingraziaRandom(text)
      var chosenFile = GetFrase(map["Sputtana"], "Sputtana")
      //var chosenFile:any = PickImg()
      if (chosenFile.type == 1) {
        await ctx.replyWithPhoto("https://cagnocammello.com/TelegramBot/"+chosenFile.path, {caption:text, reply_to_message_id: latestMessage.message_id})
      }
      else if (chosenFile.type == 2){
        await ctx.replyWithAnimation("https://cagnocammello.com/TelegramBot/"+chosenFile.path, {caption:text, reply_to_message_id: latestMessage.message_id})
      }
      else if (chosenFile.type == 3){
        await ctx.replyWithVideo("https://cagnocammello.com/TelegramBot/"+chosenFile.path, {caption:text, reply_to_message_id: latestMessage.message_id})
      }

  }
}
bot.command("sputtana", async (ctx) =>{
  await sputtana(ctx)
})
bot.command("preferiscilaido", async (ctx) =>{
    var text = GetFrase(map["PreferisciLaido"], "PreferisciLaido")
    var buffer1 = "🔵 " + text.frase1
    var buffer2 = "🔴 " + text.frase2
    text = "Preferiresti:"
    text = RingraziaRandom(text)
    await bot.api.sendPoll(
        ctx.chat.id,
        text,
        [
            buffer1,
            buffer2
        ],
        {
            is_anonymous: false,
            reply_to_message_id: ctx.msg.message_id
        }
    )
})

var latestMessage: Message
bot.on("message", async (ctx) =>{
    if (ChatIsAllowed(ctx.chat.id)){
            latestMessage = ctx.msg
        var number =GetRandom(100)
        if (number >= 27 && number <=27) {
            if (latestMessage.message_id){
                var text = "/punizione"
                var message = await ctx.replyWithHTML(text, {
                    reply_to_message_id: latestMessage.message_id,
                })
                var text = Warn(message)
                await ctx.replyWithHTML(text)
            }
        }
        if (number >= 46 && number <=46) {
            if (latestMessage.message_id){
                var text = "/sputtana"
                var message = await ctx.replyWithHTML(text, {
                    reply_to_message_id: latestMessage.message_id,
                })
                await sputtanaBot(ctx)
            }
        }
    }

})

bot.start();
