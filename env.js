"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var envalid_1 = require("envalid");
var dotenv_1 = __importDefault(require("dotenv"));
dotenv_1["default"].config();
exports["default"] = (0, envalid_1.cleanEnv)(process.env, {
    BOT_TOKEN: (0, envalid_1.str)({ desc: 'The Telegram bot API token' })
});
