"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var grammy_1 = require("grammy");
var env_1 = __importDefault(require("./env"));
var parse_mode_1 = require("@grammyjs/parse-mode");
var fs_1 = __importDefault(require("fs"));
var map = {};
map['DomandeLaide'] = JSON.parse('' + fs_1["default"].readFileSync('./DomandeLaide.json'));
map['PreferisciLaido'] = JSON.parse('' + fs_1["default"].readFileSync('./PreferisciLaido.json'));
map['Warns'] = JSON.parse('' + fs_1["default"].readFileSync('./Warns.json'));
map['Sputtana'] = JSON.parse('' + fs_1["default"].readFileSync('./Sputtana.json'));
// Create an instance of the `Bot` class and pass your bot token to it.
var bot = new grammy_1.Bot(env_1["default"].BOT_TOKEN); // <-- put your bot token between the ""
bot.use(parse_mode_1.hydrateReply);
//bot.api.config.use(parseMode("MarkdownV2"));
function GetRandom(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function GetFrase(array, filename) {
    var length = Object.keys(array).length - 1;
    var value = GetRandom(length);
    if (array[value].utilizzi && array[value].utilizzi > array[length].utilizzi) {
        var minVal = array[length].utilizzi;
        var i = 1;
        while (array[i].utilizzi > minVal) {
            i += 1;
        }
        if (i != length) {
            value = i;
        }
        else {
            array[length].utilizzi += 1;
        }
    }
    var text = array[value].frase;
    array[value].utilizzi = array[value].utilizzi == undefined ? 1 : array[value].utilizzi + 1;
    fs_1["default"].writeFileSync(filename + ".json", JSON.stringify(array, null, 4));
    map['' + filename] = JSON.parse('' + fs_1["default"].readFileSync(filename + '.json'));
    return text;
}
function RingraziaRandom(text) {
    if (GetRandom(100) <= 7) {
        text = text + "\n\nMolte delle domande di questo bot sono fornite da @BonciBo, ringrazialo!";
    }
    if (GetRandom(100) <= 7) {
        text = text + "\n\nQuesto bot è sviluppato da @Anulo, ringrazialo!";
    }
    return text;
}
function ChatIsAllowed(id) {
    if (id == -1001060910179 || id == 452798212) {
        return true;
    }
    return false;
}
bot.command("help", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, ctx.reply("Questo bot ha varie funzionalità, per ora quelle disponibili sono:\n /domandalaida - alla scoperta del tuo lato hot 😏", {
                    reply_to_message_id: ctx.msg.message_id
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
bot.command("start", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, ctx.reply("Benvenuto in questo bot dalle disparate funzionalità.\n Scrivi /help per saperne di più", {
                    reply_to_message_id: ctx.msg.message_id
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
bot.command("domandalaida", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var text;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                text = GetFrase(map["DomandeLaide"], "DomandeLaide");
                text = RingraziaRandom(text);
                return [4 /*yield*/, ctx.reply(text, {
                        reply_to_message_id: ctx.msg.message_id
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
function PickImg() {
    var files_names = [];
    var files_types = [];
    var images = fs_1["default"].readdirSync('./images');
    images.forEach(function (element) {
        files_names.push('images/' + element);
        files_types.push(1);
    });
    var gifs = fs_1["default"].readdirSync('./gifs');
    gifs.forEach(function (element) {
        files_names.push('gifs/' + element);
        files_types.push(2);
    });
    var videos = fs_1["default"].readdirSync('./videos');
    videos.forEach(function (element) {
        files_names.push('videos/' + element);
        files_types.push(3);
    });
    var chosenIndex = Math.floor(Math.random() * files_names.length);
    var chosenFile = {
        name: files_names[chosenIndex],
        type: files_types[chosenIndex]
    };
    return chosenFile;
}
function Warn(msg) {
    var text = GetFrase(map["Warns"], "Warns");
    text = " " + text;
    if (msg.reply_to_message) {
        if (msg.reply_to_message.from) {
            var id = String(msg.reply_to_message.from.id);
            if (msg.reply_to_message.from.first_name) {
                var buffer = msg.reply_to_message.from.first_name;
            }
            else if (msg.reply_to_message.from.username) {
                var buffer = msg.reply_to_message.from.username;
            }
            else {
                var buffer = id;
            }
            text = "<a href=\"tg://user?id=" + id + "\">" + buffer + "</a>" + text;
        }
    }
    else {
        text = "Utente non trovato";
    }
    text = RingraziaRandom(text);
    return text;
}
function Cinghia(msg) {
    var text = " è stato preso a cinghiate!";
    if (msg.reply_to_message) {
        if (msg.reply_to_message.from) {
            var id = String(msg.reply_to_message.from.id);
            if (msg.reply_to_message.from.first_name) {
                var buffer = msg.reply_to_message.from.first_name;
            }
            else if (msg.reply_to_message.from.username) {
                var buffer = msg.reply_to_message.from.username;
            }
            else {
                var buffer = id;
            }
            text = "<a href=\"tg://user?id=" + id + "\">" + buffer + "</a>" + text;
        }
    }
    else {
        text = "Utente non trovato";
    }
    text = RingraziaRandom(text);
    return text;
}
bot.command("cinghia", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var text;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!ChatIsAllowed(ctx.msg.chat.id)) return [3 /*break*/, 2];
                text = Cinghia(ctx.msg);
                return [4 /*yield*/, ctx.replyWithHTML(text)];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); });
bot.command("premia", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var text;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!ChatIsAllowed(ctx.msg.chat.id)) return [3 /*break*/, 2];
                text = Warn(ctx.msg);
                return [4 /*yield*/, ctx.replyWithHTML(text)];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); });
bot.command("punizione", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var text;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!ChatIsAllowed(ctx.msg.chat.id)) return [3 /*break*/, 2];
                text = Warn(ctx.msg);
                return [4 /*yield*/, ctx.replyWithHTML(text)];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2: return [2 /*return*/];
        }
    });
}); });
function sputtana(ctx) {
    return __awaiter(this, void 0, void 0, function () {
        var text, chosenFile;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!ChatIsAllowed(ctx.msg.chat.id)) return [3 /*break*/, 8];
                    text = "";
                    if (!ctx.msg.reply_to_message) {
                        text = "Utente non trovato";
                    }
                    text = RingraziaRandom(text);
                    if (!!ctx.msg.reply_to_message) return [3 /*break*/, 2];
                    return [4 /*yield*/, ctx.replyWithHTML(text)];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
                case 2:
                    chosenFile = GetFrase(map["Sputtana"], "Sputtana");
                    if (!(chosenFile.type == 1)) return [3 /*break*/, 4];
                    return [4 /*yield*/, ctx.replyWithPhoto("https://cagnocammello.com/TelegramBot/" + chosenFile.path, { caption: text })];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 8];
                case 4:
                    if (!(chosenFile.type == 2)) return [3 /*break*/, 6];
                    return [4 /*yield*/, ctx.replyWithAnimation("https://cagnocammello.com/TelegramBot/" + chosenFile.path, { caption: text })];
                case 5:
                    _a.sent();
                    return [3 /*break*/, 8];
                case 6:
                    if (!(chosenFile.type == 3)) return [3 /*break*/, 8];
                    return [4 /*yield*/, ctx.replyWithVideo("https://cagnocammello.com/TelegramBot/" + chosenFile.path, { caption: text })];
                case 7:
                    _a.sent();
                    _a.label = 8;
                case 8: return [2 /*return*/];
            }
        });
    });
}
function sputtanaBot(ctx) {
    return __awaiter(this, void 0, void 0, function () {
        var text, chosenFile;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!ChatIsAllowed(ctx.msg.chat.id)) return [3 /*break*/, 6];
                    text = "";
                    text = RingraziaRandom(text);
                    chosenFile = GetFrase(map["Sputtana"], "Sputtana");
                    if (!(chosenFile.type == 1)) return [3 /*break*/, 2];
                    return [4 /*yield*/, ctx.replyWithPhoto("https://cagnocammello.com/TelegramBot/" + chosenFile.path, { caption: text, reply_to_message_id: latestMessage.message_id })];
                case 1:
                    _a.sent();
                    return [3 /*break*/, 6];
                case 2:
                    if (!(chosenFile.type == 2)) return [3 /*break*/, 4];
                    return [4 /*yield*/, ctx.replyWithAnimation("https://cagnocammello.com/TelegramBot/" + chosenFile.path, { caption: text, reply_to_message_id: latestMessage.message_id })];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 6];
                case 4:
                    if (!(chosenFile.type == 3)) return [3 /*break*/, 6];
                    return [4 /*yield*/, ctx.replyWithVideo("https://cagnocammello.com/TelegramBot/" + chosenFile.path, { caption: text, reply_to_message_id: latestMessage.message_id })];
                case 5:
                    _a.sent();
                    _a.label = 6;
                case 6: return [2 /*return*/];
            }
        });
    });
}
bot.command("sputtana", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, sputtana(ctx)];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
bot.command("preferiscilaido", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var text, buffer1, buffer2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                text = GetFrase(map["PreferisciLaido"], "PreferisciLaido");
                buffer1 = "🔵 " + text.frase1;
                buffer2 = "🔴 " + text.frase2;
                text = "Preferiresti:";
                text = RingraziaRandom(text);
                return [4 /*yield*/, bot.api.sendPoll(ctx.chat.id, text, [
                        buffer1,
                        buffer2
                    ], {
                        is_anonymous: false,
                        reply_to_message_id: ctx.msg.message_id
                    })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
var latestMessage;
bot.on("message", function (ctx) { return __awaiter(void 0, void 0, void 0, function () {
    var number, text, message, text, text, message;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!ChatIsAllowed(ctx.chat.id)) return [3 /*break*/, 6];
                latestMessage = ctx.msg;
                number = GetRandom(100);
                if (!(number >= 27 && number <= 27)) return [3 /*break*/, 3];
                if (!latestMessage.message_id) return [3 /*break*/, 3];
                text = "/punizione";
                return [4 /*yield*/, ctx.replyWithHTML(text, {
                        reply_to_message_id: latestMessage.message_id
                    })];
            case 1:
                message = _a.sent();
                text = Warn(message);
                return [4 /*yield*/, ctx.replyWithHTML(text)];
            case 2:
                _a.sent();
                _a.label = 3;
            case 3:
                if (!(number >= 46 && number <= 46)) return [3 /*break*/, 6];
                if (!latestMessage.message_id) return [3 /*break*/, 6];
                text = "/sputtana";
                return [4 /*yield*/, ctx.replyWithHTML(text, {
                        reply_to_message_id: latestMessage.message_id
                    })];
            case 4:
                message = _a.sent();
                return [4 /*yield*/, sputtanaBot(ctx)];
            case 5:
                _a.sent();
                _a.label = 6;
            case 6: return [2 /*return*/];
        }
    });
}); });
bot.start();
